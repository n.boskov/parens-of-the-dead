(defproject parens-of-the-dead "0.1.0-SNAPSHOT"
  :description "A Series of browser games written in clojure."
  :url ""
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [http-kit "2.1.18"]
                 [com.stuartsierra/component "0.3.0"]
                 [compojure "1.4.0"]
                 [org.clojure/clojurescript "1.7.122"]
                 [quiescent "0.2.0-RC2"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [expectations "2.1.3"]
                 [com.cemerick/piggieback "0.2.1"]
                 [jarohen/chord "0.6.0"]]
  :plugins []
  :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
  :main ^:skip-aot undead.system
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:plugins [[lein-cljsbuild "1.0.6"]
                             [lein-figwheel "0.3.7"]
                             [lein-expectations "0.0.8"]
                             [lein-autoexpect "1.6.0"]]
                   :dependencies [[reloaded.repl "0.2.0"]]
                   :source-paths ["dev"]
                   :cljsbuild {:builds [{:source-paths ["src" "dev"]
                                         :figwheel true
                                         :compiler {:output-to "resources/public/app.js"
                                                    :output-dir "resources/public/out"
                                                    :optimizations :none
                                                    :recompile-dependents true
                                                    :source-map true}}]}}})
